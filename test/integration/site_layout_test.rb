require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links when signed out" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    # See http://stackoverflow.com/questions/41038302/integration-test-failing-for-footer-links
    # Commented until a solution becomes obvious
    # assert_select "a[href=?]", about_path
    # assert_select "a[href=?]", contact_path
    assert_select "a[href=?]",  login_path
    get contact_path
    assert_select "title", full_title("Contact")

    get signup_path
    assert_template 'users/new'
  end

  test "layout links when signed in" do
    @user = users :michael
    log_in_as @user
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", help_path, count: 1
    assert_select "a[href=?]", users_path, count: 1
    assert_select "a[href=?]", user_path(@user), count: 1
    assert_select "a[href=?]", edit_user_path(@user), count: 1
    assert_select "a[href=?]", logout_path, count: 1
  end
end
